package ru.edu.model;

import org.junit.Test;

import java.sql.Date;
import java.time.Instant;
import java.util.UUID;

import static org.junit.Assert.*;

public class StudentTest {
    Student student = new Student();

    @Test
    public void studentTest() {
    UUID id = UUID.randomUUID();
    student.setId(id);
    student.setFirstName("first");
    student.setLastName("last");
    student.setBirthDate(new Date(Instant.now().getEpochSecond()));
    student.setGraduated(false);

    assertEquals(id, student.getId());
    assertEquals("first", student.getFirstName());
    assertEquals("last", student.getLastName());
    assertEquals(new Date(Instant.now().getEpochSecond()), student.getBirthDate());
    assertEquals(false, student.isGraduated());

    }


}