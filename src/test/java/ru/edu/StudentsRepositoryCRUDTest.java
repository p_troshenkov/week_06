package ru.edu;

import org.junit.Test;

import ru.edu.model.Student;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static ru.edu.LocalDbCreation.JDBC_H_2_LOCAL_DB_DB;

public class StudentsRepositoryCRUDTest {


    @Test
    public void createTest() throws SQLException {
        StudentsRepositoryCRUDImpl crud = new StudentsRepositoryCRUDImpl(getConnection());
        Student student = new Student("first", "last", new Date(Instant.now().getEpochSecond()), false);
        UUID createdId = crud.create(student);
        Student newStudent = crud.selectById(createdId);
        assertEquals(student.getFirstName(), newStudent.getFirstName());
        assertEquals(createdId, newStudent.getId());
    }

    @Test
    public void updateTest() throws SQLException {
        StudentsRepositoryCRUDImpl crud = new StudentsRepositoryCRUDImpl(getConnection());
        Student student1 = new Student("first", "last", new Date(Instant.now().getEpochSecond()), false);
        crud.create(student1);
        student1.setFirstName("first1");
        student1.setLastName("last1");
        student1.setGraduated(true);
        int rows = crud.update(student1);
        assertEquals(1, rows);

        Student student2 = crud.selectAll().get(0);
        assertEquals(student2.getFirstName(), "first1");
        assertEquals(student2.getLastName(), "last1");
        assertEquals(student2.isGraduated(), true);
    }

    @Test (expected = IllegalStateException.class)
    public void updateTestException() throws SQLException {
        List<UUID> idList = new ArrayList<>();

        StudentsRepositoryCRUDImpl crud = new StudentsRepositoryCRUDImpl(getConnection());
        Student student1 = new Student("first", "last", new Date(Instant.now().getEpochSecond()), false);
        crud.create(student1);
        idList.add(student1.getId());
        crud.remove(idList);
        crud.update(student1);
    }


    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(JDBC_H_2_LOCAL_DB_DB, "sa", "");
    }
}