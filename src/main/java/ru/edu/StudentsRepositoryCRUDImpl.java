package ru.edu;

import ru.edu.model.Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class StudentsRepositoryCRUDImpl implements StudentsRepositoryCRUD {
    /**
     * Шаблон запроса заполнения таблицы.
     */
    private static final String INSERT_STUDENTS = "INSERT INTO "
            + "STUDENTS (id, first_name, last_name, birth_date, is_graduated) "
            + "VALUES (?, ?, ?, ?, ?)";

    /**
     * Шаблон запроса изменения данных таблицы.
     */
    private static final String UPDATE_STUDENTS = "UPDATE STUDENTS SET "
            + "first_name = ?, last_name = ?, birth_date = ?, "
            + "is_graduated = ? WHERE id = ?";

    /**
     * Шаблон запроса отображения всех данных в таблице.
     */
    private static final String SELECT_ALL_STUDENTS = "SELECT * FROM STUDENTS";

    /**
     * Шаблон запроса отображения данных по id.
     */
    private static final String SELECT_STUDENTS =
            "SELECT * FROM STUDENTS WHERE id = ?";

    /**
     * Соединение с БД.
     */
    private Connection connection;

    /**
     * Конструктор класса.
     *
     * @param connectionDb
     */
    public StudentsRepositoryCRUDImpl(final Connection connectionDb) {
        connection = connectionDb;
    }

    /**
     * Создание записи в БД.
     * id у student должен быть null, иначе требуется вызов update.
     * id генерируем через UUID.randomUUID()
     *
     * @param student
     * @return сгенерированный UUID
     */
    @Override
    public UUID create(final Student student) {
        UUID id = UUID.randomUUID();
        student.setId(id);
        int insertedRows = execute(
                INSERT_STUDENTS,
                id.toString(),
                student.getFirstName(),
                student.getLastName(),
                student.getBirthDate(),
                student.isGraduated()
        );

        if (insertedRows == 0) {
            throw new IllegalStateException("Not inserted");
        }

        return id;
    }

    /**
     * Получение записи по id из БД.
     *
     * @param id
     * @return
     */
    @Override
    public Student selectById(final UUID id) {
        return query(SELECT_STUDENTS, id.toString()).get(0);
    }

    /**
     * Получение всех записей из БД.
     *
     * @return
     */
    @Override
    public List<Student> selectAll() {
        return query(SELECT_ALL_STUDENTS);
    }

    /**
     * Обновление записи в БД.
     *
     * @param student
     * @return количество обновленных записей
     */
    @Override
    public int update(final Student student) {
        int updatedRows = execute(
                UPDATE_STUDENTS,
                student.getFirstName(),
                student.getLastName(),
                student.getBirthDate(),
                student.isGraduated(),
                student.getId().toString()
        );
        if (updatedRows == 0) {
            throw new IllegalStateException("Not updated");
        }
        return updatedRows;
    }

    /**
     * Удаление указанных записей по id.
     *
     * @param idList
     * @return количество удаленных записей
     */
    @Override
    public int remove(final List<UUID> idList) {
        String sql = "DELETE FROM STUDENTS WHERE id IN (";
        int i = 0;
        for (UUID id: idList) {
            sql += "'" + id.toString() + "'";
            if (++i != idList.size()) {
                sql += ",";
            } else {
                sql += ")";
            }
        }
        return execute(sql);
    }

    /**
     * Создание Update запроса.
     * @param sql
     * @param values
     * @return количество обработанных строк
     */
    private int execute(final String sql, final Object... values) {
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }
            return statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Создание Query запроса.
     * @param sql
     * @param values
     * @return список студентов
     */
    private List<Student> query(final String sql, final Object... values) {
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }
            ResultSet resultSet = statement.executeQuery();
            List<Student> result = new ArrayList<>();
            while (resultSet.next()) {
                result.add(map(resultSet));
            }
            return result;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Сборка объекта Student из БД.
     * @param resultSet
     * @return объект Student
     * @throws SQLException
     */
    private Student map(final ResultSet resultSet) throws SQLException {
        Student student = new Student();
        student.setId(UUID.fromString(resultSet.getString("id")));
        student.setFirstName(resultSet.getString("first_name"));
        student.setLastName(resultSet.getString("last_name"));
        student.setBirthDate(resultSet.getDate("birth_date"));
        student.setGraduated(resultSet.getBoolean("is_graduated"));
        return student;
    }


}
