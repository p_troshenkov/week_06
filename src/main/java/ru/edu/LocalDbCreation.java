package ru.edu;

import java.sql.DriverManager;
import java.sql.SQLException;

public final class LocalDbCreation {
    /**
     * Путь к базе данных.
     */
    public static final String JDBC_H_2_LOCAL_DB_DB =
            "jdbc:h2:./target/localDb/db";

    /**
     * Конструктор по умолчанию.
     */
    private LocalDbCreation() {

    }

    /**
     * Соединение с базой данных.
     * @param args
     */
    public static void main(final String[] args) throws SQLException {
        DriverManager.getConnection(JDBC_H_2_LOCAL_DB_DB, "sa", "");
    }
}
