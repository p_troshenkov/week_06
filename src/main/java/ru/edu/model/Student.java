package ru.edu.model;

import java.util.Date;
import java.util.UUID;

/**
 * Класс отражающий структуру хранимых в таблице полей.
 */
public class Student {
    /**
     * Первичный ключ.
     *
     * Рекомендуется генерировать его только внутри
     * StudentsRepositoryCRUD.create(), иными словами
     * до момента пока объект не будет сохранен в БД,
     * он не должен иметь значение id.
     */
    private UUID id;

    /**
     * Имя студента.
     */
    private String firstName;

    /**
     * Фамилия студента.
     */
    private String lastName;

    /**
     * Дата рождения студента.
     */
    private Date birthDate;

    /**
     * Окончание обучения.
     */
    private boolean isGraduated;

    /**
     * Конструктор по умолчанию.
     */
    public Student() {
    }

    /**
     * Конструктор.
     *
     * @param first
     * @param last
     * @param date
     * @param b
     */
    public Student(
            final String first,
            final String last,
            final java.sql.Date date,
            final boolean b
    ) {
        firstName = first;
        lastName = last;
        birthDate = date;
        isGraduated = b;
    }

    /**
     * Получить id.
     *
     * @return id
     */
    public UUID getId() {
        return id;
    }

    /**
     * Установить id.
     *
     * @param studentId
     */
    public void setId(final UUID studentId) {
        id = studentId;
    }
    /**
     * Получить имя.
     *
     * @return имя
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Установить имя.
     *
     * @param studentFirstName
     */
    public void setFirstName(final String studentFirstName) {
        firstName = studentFirstName;
    }

    /**
     * Получить фамилию.
     *
     * @return фамилия
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Установить фамилию.
     *
     * @param studentLastName
     */
    public void setLastName(final String studentLastName) {
        lastName = studentLastName;
    }

    /**
     * Получить дату рождения.
     *
     * @return дата рождения
     */
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * Установить дату рождения.
     *
     * @param studentBirthDate
     */
    public void setBirthDate(final Date studentBirthDate) {
        birthDate = studentBirthDate;
    }

    /**
     * Получить информацию об окончании.
     *
     * @return isGraduated
     */
    public boolean isGraduated() {
        return isGraduated;
    }

    /**
     * Установить информацию об окончании.
     *
     * @param graduated
     */
    public void setGraduated(final boolean graduated) {
        isGraduated = graduated;
    }
}
